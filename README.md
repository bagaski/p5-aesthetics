# p5-aesthetics

My learning repository while reading an following the book "Aesthetic Programming" by Geoff Cox and Winnie Soon. I will keep my exercises a and sketches here. I will try to keep them organized and easy to follow!

And this is a first sketch I created to use as RunMe, https://editor.p5js.org/bagaski/full/zccdqUJow

This is part of the 1st Chapter "Getting Started" - I will organize the rest of the chapters in different folders.



